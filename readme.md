Stores web bookmarks in a Graph database. Allows to assign multiple tags to a
URI, search by multiple tags combinations. Has web front-end.